﻿using UnityEngine;
using System.Collections;

public class PlatformerProgressController : MonoBehaviour 
{

	public GameObject currentLevel;
	public GameObject nextLevel;

	void OnTriggerEnter2D(Collider2D other)
	{
		nextLevel.SetActive (true);
		nextLevel.GetComponent<PlatformerBallReset>().SetToStartingPosition();
		currentLevel.SetActive(false);
	}

}
