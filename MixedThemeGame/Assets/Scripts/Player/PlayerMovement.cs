﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public Vector3 speed;


	void Start () 
	{
		speed = new Vector3(0,0,0);
	}

	void Update ()
	{
		gameObject.transform.position += speed;
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Footbridge")
		{
			collision.gameObject.GetComponent<FootbridgeMovement>().AttachToFootbridge(this.gameObject);
		}
	}

	void OnCollisionExit2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Footbridge")
		{
			collision.gameObject.GetComponent<FootbridgeMovement>().DetachFromFootbridge();
		}
	}
}
