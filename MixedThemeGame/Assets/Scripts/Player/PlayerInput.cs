﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour 
{
	public PlayerMovement playerMovement;
	float horizontalSpeed = 4;

	public GameObject level1Reference;
	public GameObject level2Reference;
	public GameObject level3Reference;
	GameObject currentLevel;

	private static PlayerInput pseudoSingleton;
	public static PlayerInput Get()
	{
		return pseudoSingleton;
	}

	void Awake()
	{
		pseudoSingleton = this;
	}

	void FixedUpdate () 
	{
		rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);

		if (Input.GetKey ("up") || Input.GetKey("w"))
		{
			if(IsOnGround())
				rigidbody2D.AddForce(new Vector2(0,20000));
		}

		if (Input.GetKey ("left") || Input.GetKey("a"))			playerMovement.speed.x = -horizontalSpeed;
		else if (Input.GetKey ("right") || Input.GetKey("d"))	playerMovement.speed.x = horizontalSpeed;
		else 													playerMovement.speed.x = 0;
		
	}

	public bool IsUnderFallingObject()
	{
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up);
		if (hit.collider != null)
		{
			if(hit.collider.gameObject.name == "Falling")
				if(hit.distance < 20)
					return true;
		}
		
		return false;
	}

	private bool IsOnGround()
	{
		RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up);
		if (hit.collider != null)
		{
			if(hit.collider.gameObject.tag == "Floor" || hit.collider.gameObject.tag == "Footbridge")
				if(hit.distance < 20)
					return true;
		}

		return false;
	}

	public void SetCurrentLevel(GameObject currentLevelReference)
	{
		currentLevel = currentLevelReference;
	}
	
	void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "Damaging")
		{
			level1Reference.SetActive (false);
			level2Reference.SetActive(false);
			level3Reference.SetActive(false);

			currentLevel.SetActive(true);
			currentLevel.GetComponent<PlatformerBallReset>().SetToStartingPosition();
		}
	}
}
