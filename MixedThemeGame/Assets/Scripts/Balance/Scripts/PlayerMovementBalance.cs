﻿using UnityEngine;
using System.Collections;

public class PlayerMovementBalance : MonoBehaviour {
	
	public GameCompleteBalance gameCompleteBalance;
	public GameOverBalance gameOverBalance;

	Vector3 startingPosition;

	void Awake () {
		startingPosition = gameObject.transform.position;
	}

	public void ResetBall()
	{
		gameObject.transform.position = startingPosition;
	}


	void OnCollisionEnter (Collision col)
	{
		if(col.gameObject.tag == "Respawn")
		{
			col.gameObject.SetActive(false);
			gameCompleteBalance.gameObject.transform.parent.gameObject.transform.parent.gameObject.SetActive(true);
			gameObject.transform.parent.gameObject.SetActive (false);
			//Destroy(col.gameObject);
		}
	}
	void FixedUpdate() 
	{
		float speed = 5;
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 position = gameObject.transform.position;

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rigidbody.AddForce (movement * speed);

		gameObject.transform.position = position;

		if (position.y < -40) 
		{
			gameOverBalance.gameObject.transform.parent.gameObject.transform.parent.gameObject.SetActive(true);
			//gameObject.transform.parent.gameObject.SetActive (false);
		}


	}

}
