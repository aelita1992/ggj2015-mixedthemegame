﻿using UnityEngine;
using System.Collections;

public class FootbridgeMovement : MonoBehaviour {

	public Vector3 speed;

	public enum Direction {FirstStage, SecondStage, PrepareFirstStage, PrepareSecondStage};
	public Direction direction;

	public float distance;
	public float distanceLimit;

	float timer;
	GameObject attachedPlayer;

	void Start () 
	{
		attachedPlayer = null;
	}

	void Update () 
	{
		if(direction == Direction.FirstStage || direction == Direction.SecondStage)
			MovePlatform();

		if(distance >= distanceLimit)
		{
			distance = 0;

			if(direction == Direction.FirstStage)
				direction = Direction.PrepareSecondStage;
			else
				direction = Direction.PrepareFirstStage;

			timer = 2;
		}

		if(direction == Direction.PrepareFirstStage || direction == Direction.PrepareSecondStage)
			PrepareMovement();

	}

	private void PrepareMovement()
	{
		timer -= Time.deltaTime;

		if(timer <= 0)
		{
			if(direction == Direction.PrepareFirstStage)
				direction = Direction.FirstStage;

			if(direction == Direction.PrepareSecondStage)
				direction = Direction.SecondStage;
		}
	}

	private void MovePlatform()
	{
		if(direction == Direction.FirstStage)
		{
			gameObject.transform.position -= speed;
			distance += Mathf.Abs(speed.x) + Mathf.Abs(speed.y);

			if(attachedPlayer != null)
				attachedPlayer.transform.position -= speed;
		}

		if(direction == Direction.SecondStage)
		{
			gameObject.transform.position += speed;
			distance += Mathf.Abs(speed.x) + Mathf.Abs(speed.y);

			if(attachedPlayer != null)
				attachedPlayer.transform.position += speed;
		}
	}

	public void AttachToFootbridge(GameObject player)
	{
		attachedPlayer = player;
	}

	public void DetachFromFootbridge()
	{
		attachedPlayer = null;
	}
}
