﻿using UnityEngine;
using System.Collections;

public class MainGamePanel : MonoBehaviour
{

	public void Expand()
	{
		gameObject.SetActive(true);
	}

	public void Collapse()
	{
		gameObject.SetActive(false);
	}
}
