﻿using UnityEngine;
using System.Collections;

public class SettingsPanel : MonoBehaviour 
{
	public UIButton returnButton;

	void Awake()
	{

	}

	public void Expand()
	{
		gameObject.SetActive(true);
	}
	
	public void Collapse()
	{
		gameObject.SetActive(false);
	}
}
