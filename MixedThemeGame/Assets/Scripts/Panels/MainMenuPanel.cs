﻿	using UnityEngine;
using System.Collections;

public class MainMenuPanel : MonoBehaviour 
{
	public UIButton start;
	public UIButton settings;
	public UIButton exit;

	public void Expand()
	{
		gameObject.SetActive(true);
	}

	public void Collapse()
	{
		gameObject.SetActive(false);
	}
}
