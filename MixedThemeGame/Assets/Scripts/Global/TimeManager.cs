﻿using UnityEngine;

public class TimeManager
{
	static TimeManager instance;
	
	public delegate void TimeListener();	
	public event TimeListener timeDependantUpdate;
	public event TimeListener timeDependantUpdate_1second;
	public event TimeListener timeDependantUpdate_10seconds;

	bool gamePaused;
	float oneSecondTimer = 1;
	float tenSecondsTimer = 10;
    public float elapsedTime = 0f;
    float lastDelta = 0f;
	
	public void CustomUpdate()
	{
		if(!gamePaused)
		{
            elapsedTime += Time.deltaTime;
			oneSecondTimer -= Time.deltaTime;
			tenSecondsTimer -= Time.deltaTime;
			
			if(tenSecondsTimer <= 0)
			{
				if(timeDependantUpdate_10seconds != null) 
					timeDependantUpdate_10seconds();

				tenSecondsTimer = 10;
			}

			if(oneSecondTimer <= 0)
			{
				if(timeDependantUpdate_1second != null)
					timeDependantUpdate_1second();

					oneSecondTimer = 1;
			}
				

			if(timeDependantUpdate != null)	
				timeDependantUpdate();
		}
	}

	public void EnableBulletTime()
	{
		Time.timeScale = 0.5f;
	}

	public void DisableBulletTime()
	{
		Time.timeScale = 1f;
	}

	public void PauseGame()
	{
		gamePaused = true;
		Debug.LogError ("Paused");
	}
	
	public void ResumeGame()
	{	
		gamePaused = false;
		Debug.LogError ("Resumed");
	}
	
	public void Reset()
	{
		gamePaused = false;
	}
	
	public static TimeManager Get()
	{
		if(instance == null)
			instance = new TimeManager();
		
		return instance;
	}
}