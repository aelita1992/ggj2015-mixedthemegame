﻿using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour 
{
	protected abstract class State
	{
		protected StateMachine stateMachineAccess;

		public State()
		{
			this.stateMachineAccess = StateMachine.Get();
		}

		public abstract void Init();
		public virtual void Exit(){}
	}

    //-------------------------------------------------------------

    class MainMenuState : State
    {
		MainMenuPanel mainMenuPanel;

        public override void Init()
        {
			TimeManager.Get ().PauseGame();
			mainMenuPanel = stateMachineAccess.mainMenuPanel;

			mainMenuPanel.Expand();

			mainMenuPanel.start.OnButtonPressed += OnStart;
			mainMenuPanel.settings.OnButtonPressed += OnSettings;
			mainMenuPanel.exit.OnButtonPressed += OnExit;
        }

        public override void Exit()
        {
			mainMenuPanel.Collapse();

			mainMenuPanel.start.OnButtonPressed -= OnStart;
			mainMenuPanel.settings.OnButtonPressed -= OnSettings;
			mainMenuPanel.exit.OnButtonPressed -= OnExit;
        }

		private void OnStart()
		{
			StateMachine.Get ().SetState(new MainGameState());
		}

		private void OnSettings()
		{
			StateMachine.Get ().SetState(new SettingsState());
		}

		private void OnExit()
		{
			Application.Quit ();
		}
    }

	class SettingsState : State
	{
		SettingsPanel settingsPanel;

		public override void Init()
		{
			settingsPanel = stateMachineAccess.settingsPanel;

			settingsPanel.Expand();
			settingsPanel.returnButton.OnButtonPressed += OnReturn;
		}
		
		public override void Exit()
		{
			settingsPanel.returnButton.OnButtonPressed -= OnReturn;
			settingsPanel.Collapse();
		}

		private void OnReturn()
		{
			StateMachine.Get ().SetState(new MainMenuState());
		}
	}

	class MainGameState : State
	{
		MainGamePanel mainGamePanel;

		public override void Init()
		{
			mainGamePanel = stateMachineAccess.mainGamePanel;

			mainGamePanel.Expand();
			TimeManager.Get ().ResumeGame();
		}
		
		public override void Exit()
		{
			mainGamePanel.Collapse();
		}
		
	}

    //-------------------------------------------------------------

	public MainGamePanel mainGamePanel;
	public MainMenuPanel mainMenuPanel;
	public SettingsPanel settingsPanel;

	static StateMachine instance;

	State currentState;

	static public StateMachine Get() { return instance;}

	void Awake()
	{
		if(StateMachine.instance == null)
			StateMachine.instance = this;
		else
			Debug.LogError("Error with the state machine's singleton");
	}

	void Start()
	{
		Init();
	}

	protected void SetState(State newState)
	{
		if(currentState != null)
			currentState.Exit ();
		currentState = newState;
		newState.Init ();
	}

	public void Init()
	{
		StateMachine.Get ().SetState(new MainGameState());
	}

}
