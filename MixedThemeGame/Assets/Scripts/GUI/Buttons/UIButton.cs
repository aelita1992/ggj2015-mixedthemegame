﻿using UnityEngine;
using System.Collections;

public class UIButton : MonoBehaviour {

	public delegate void ClickAction();
	public event ClickAction OnButtonPressed;

	public enum ButtonType {ForwardButton, ReturnButton}
	public ButtonType buttonType;

	UILabel label; 

	void Awake () 
	{
		label = GetComponentInChildren<UILabel>();
	}
	
	void OnEnable() 
	{

	}
	
	void OnMouseEnter() 
	{
		label.SetActivenessInfo(true);
	}
	
	void OnMouseExit() 
	{
		label.SetActivenessInfo(false);
	}
	
	void OnMouseDown()
	{
		if(OnButtonPressed != null)
		{
			/*if(buttonType == ButtonType.ForwardButton)
				AudioManager.Get ().PlayEffectClip(AudioManager.EffectClipName.BUTTON1);

			if(buttonType == ButtonType.ReturnButton)
				AudioManager.Get ().PlayEffectClip(AudioManager.EffectClipName.BUTTON2);
*/
			OnButtonPressed();
		}
	}
}
