﻿using UnityEngine;
using System.Collections;

public class OptionsSlider : MonoBehaviour 
{
	Vector3 startPosition;
	
	bool isDragged;
	Vector3 oldMousePosition;

//	public AudioManager audioManager;
//	public AudioManager.VolumeEnum volumeGroup;
	public Camera mainCamera;
	public TextMesh volumeValueLabel;
	
	void Start()
	{
		startPosition = gameObject.transform.position;
	}
	
	public void ResetPosition()
	{
		isDragged = false;
		gameObject.transform.position = startPosition;
	}
	
	void OnMouseDown()
	{
		isDragged = true;
		//ChangeMusic();
	}
	
	void OnMouseUp()
	{
		isDragged = false;
//		AudioManager.Get ().PlayMusicClip(AudioManager.MusicClipName.MAIN_MENU_THEME);
	}

	void ChangeMusic()
	{
	/*	switch(volumeGroup)
		{
		case AudioManager.VolumeEnum.MUSIC:
			AudioManager.Get ().PlayMusicClip(AudioManager.MusicClipName.INGAME_THEME);
			break;

		case AudioManager.VolumeEnum.EFFECTS:
			AudioManager.Get ().PlayEffectClip(AudioManager.EffectClipName.MISSILE_ALERT);
			break;
		}*/
	}
	
	void Update()
	{
		if(!isDragged)
			return;
		
		Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
		Vector3 currentPosition = this.transform.position;

		currentPosition.x = mousePosition.x - 1000;

		if(currentPosition.x < -1240)
			currentPosition.x = -1240;

		if(currentPosition.x > -760)
			currentPosition.x = -760;

		this.transform.position = currentPosition;

		float fraction = (currentPosition.x + 1240)/480;
//		audioManager.SetVolume(volumeGroup, fraction);

		fraction *= 100;
		if(volumeValueLabel != null)
			volumeValueLabel.text = (int)fraction + " %";
	}
	
}
