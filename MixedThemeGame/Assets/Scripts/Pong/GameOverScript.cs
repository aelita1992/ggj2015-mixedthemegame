﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverScript : MonoBehaviour {

	Text text;
	float timer = 1;
	bool isRed = true;

	void Awake () {
		text = GetComponent <Text> ();

	}

	

	void Update () 
	{
		timer -= Time.deltaTime;

		if (timer <= 0)
		{	
			if(isRed)
			{
				text.color = Color.gray;
			}
			else
			{
				text.color = Color.red;
			}
			timer = 1;
			isRed = !isRed;
		}
	}
}
