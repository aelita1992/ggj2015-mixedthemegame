﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CountdownScript : MonoBehaviour {

	Text text;
	public static float countdown;
	public BallMovement ballMovement;
	public GameOverSuccessfullyScript gameOverSuccessfullyScript;

 
	void Awake()
	{
		text = GetComponent <Text> ();
		countdown = 20;

	}
	
	void Update () {
			countdown -= Time.deltaTime;
		text.text = ((int)countdown).ToString();
		Color color = text.color;
		color.a = 0.5f;
		text.color = color;
		if (countdown < 0)
		{
			countdown = 0;
			//gameOverSuccessfullyScript.gameObject.transform.parent.gameObject.transform.parent.gameObject.SetActive (true);
			//ballMovement.gameObject.transform.parent.gameObject.SetActive (false);
		}
	}
}
