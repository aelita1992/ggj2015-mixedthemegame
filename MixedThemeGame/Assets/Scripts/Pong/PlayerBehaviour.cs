﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
	public float yMin, yMax;
}

public class PlayerBehaviour : MonoBehaviour {

	void Start () 
	{
	
	}

	public Boundary boundary;

	void Update () 
	{
		float speed = 500;
		bool up = Input.GetKey (KeyCode.W);
		bool down = Input.GetKey (KeyCode.S); 
		bool upArrow = Input.GetKey (KeyCode.UpArrow);
		bool downArrow = Input.GetKey (KeyCode.DownArrow);


		Vector3 position = gameObject.transform.position;
		
		if (up || upArrow) 
		{
			position.y += speed * Time.deltaTime;
		}
		
		if (down || downArrow) 
		{
			position.y -= speed * Time.deltaTime;
		} 

		position = new Vector3 
			(
				-375.0f, Mathf.Clamp (position.y, boundary.yMin, boundary.yMax), -5.0f);


		gameObject.transform.position = position;
	}


}
