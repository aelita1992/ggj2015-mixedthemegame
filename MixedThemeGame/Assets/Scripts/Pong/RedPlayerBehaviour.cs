﻿using UnityEngine;
using System.Collections;

public class RedPlayerBehaviour : MonoBehaviour {

	public BallMovement ballMovement;

	void Start () 
	{

	}

	public Boundary boundary;

	void Update () 
	{
		float speed = 300;
		Vector3 position = gameObject.transform.position;
		Vector3 ballPosition = ballMovement.gameObject.transform.position;

		if(CountdownScript.countdown < 1)
		{
			position.y += speed * Time.deltaTime;
			position = new Vector3 
				(375.0f, Mathf.Clamp (position.y, boundary.yMin, boundary.yMax), -5.0f);
			gameObject.transform.position = position;

			return;
		}
		
		if (ballPosition.x > -200 && position.y < ballPosition.y) 
		{
			position.y += speed * Time.deltaTime;
		}

		if (ballPosition.x > -200 && position.y > ballPosition.y)
		{
			position.y -= speed * Time.deltaTime;
		}

		//bool move =true;


		
			position = new Vector3 
			(
				375.0f, Mathf.Clamp (position.y, boundary.yMin, boundary.yMax), -5.0f);

		//}

		gameObject.transform.position = position;
	}
}
