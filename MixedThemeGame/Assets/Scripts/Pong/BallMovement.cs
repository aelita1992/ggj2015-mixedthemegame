﻿using UnityEngine;
using System.Collections;

public class BallMovement : MonoBehaviour {

	Vector3 startingPosition;

	public Vector3 position;
	public Boundary boundary;
	public float statusY = 1;
	public float statusX = 1;
	public float speed = 600;
	public GameOverScript gameOverScript;

	float gameInitTimer;
	public WhatsNextController whatsNext;

	void Awake () 
	{
		startingPosition = gameObject.transform.position;
		gameInitTimer = 1;
	}

	public void ResetBall()
	{
		gameObject.transform.position = startingPosition;
		statusY = 1;
		statusX = 1;
		gameInitTimer = 1;
	}

	void Update () 
	{
		if(gameInitTimer > 0)
		{
			gameInitTimer -= Time.deltaTime;
			return;
		}
		position = gameObject.transform.position;

		if (position.y > 275)
		{
			statusY = -1;
			position.y += statusY * speed * Time.deltaTime;
		}
		else if (position.y < -275)
		{
			statusY = 1;
			position.y += statusY * speed * Time.deltaTime;
		}
		else 
		{
		position.y += statusY * speed * Time.deltaTime;
		position.x += statusX * speed * Time.deltaTime;
		}

		if (position.x < -400) 
		{
			GameOver();
		}

		if(position.x > 400)
		{
			GameWon ();
		}

	//	OnCollisionEnter ();
		gameObject.transform.position = position;
	}

	void GameOver()
	{
		gameOverScript.gameObject.transform.parent.gameObject.transform.parent.gameObject.SetActive (true);
		gameObject.transform.parent.gameObject.SetActive (false);
	}

	void GameWon()
	{
		whatsNext.gameObject.SetActive(true);
		whatsNext.ShowWinScreen();
		gameObject.transform.parent.gameObject.SetActive (false);
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.collider.name == "GreenPlayer")
		{
			statusX = 1;
			position.x += statusX * speed * Time.deltaTime;
		}

		else if(col.collider.name == "RedPlayer")
		{
			statusX = -1;
			position.x += statusX * speed * Time.deltaTime;
		}
	}
}
