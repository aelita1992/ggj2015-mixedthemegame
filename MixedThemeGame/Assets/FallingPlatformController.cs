﻿using UnityEngine;
using System.Collections;

public class FallingPlatformController : MonoBehaviour 
{
	Vector3 speed;
	float distance;

	enum FallingStage {IDLE, UP, DOWN, DEAD}
	FallingStage stage;
	float timer;

	Vector3 startingPosition;

	void Awake()
	{
		startingPosition = gameObject.transform.position;
	}

	void Start()
	{
		distance = 0;
		timer = 1;
	}

	void OnCollisionEnter2D(Collision2D coll) 
	{
		if(coll.gameObject.tag == "Player")
		{
			if(PlayerInput.Get ().IsUnderFallingObject())
				return;

			stage = FallingStage.UP;
		}
	}

	public void Reset()
	{
		gameObject.GetComponent<SpriteRenderer>().enabled = true;
		gameObject.GetComponent<BoxCollider2D>().enabled = true;
		gameObject.transform.position = startingPosition;
		stage = FallingStage.IDLE;
		timer = 1;
	}

	void Update()
	{
		if(stage == FallingStage.UP)
		{
			timer -= Time.deltaTime;

			if(timer > 0)
				return;

			gameObject.transform.position += new Vector3(0, 2, 0);
			distance += 2;

			if(distance >= 10)
			{
				distance = 0;
				stage = FallingStage.DOWN;
			}
		}

		if(stage == FallingStage.DOWN)
		{
			gameObject.transform.position -= new Vector3(0, 2, 0);
			distance += 2;

			if(distance >= 100)
			{
				stage = FallingStage.DEAD;
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
				gameObject.GetComponent<BoxCollider2D>().enabled = false;
				timer = 2;
			}
		}

		if(stage == FallingStage.DEAD)
		{
			if(timer > 0)
				timer -= Time.deltaTime;

			if(timer <= 0)
			{
				Reset();
			}
		}


	}
}
