﻿using UnityEngine;
using System.Collections;

public class AK47Controller : MonoBehaviour
{
	public ParticleSystem hitParticleSystem;
	float firingCooldownLeft;
	float firingStartingCooldown = 0.2f;

	public AudioSource ak47Source;
	public AudioClip ak47Clip;
	bool isPlaying;

	void Start()
	{
		isPlaying = false;
		firingCooldownLeft = firingStartingCooldown;
	}

	public void Fire()
	{
		if(!isPlaying)
		{
			isPlaying = true;
			ak47Source.Play();

			Invoke ("ResetIsPlaying", ak47Clip.length);
		}

		firingCooldownLeft -= Time.deltaTime;
		if(firingCooldownLeft > 0)
			return;

		Vector3 direction = Camera.main.transform.forward;
		Ray ray = new Ray(PlayerController_FPS.Get ().gameObject.transform.position, direction);

		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, 4000.0F))
		{
			hitParticleSystem.transform.position = hit.point;
			hitParticleSystem.Play();

			if(hit.collider.gameObject.tag == "Tower")
				Destroy (hit.collider.gameObject);
		}

		firingCooldownLeft = firingStartingCooldown;
	}

	private void ResetIsPlaying()
	{
		isPlaying = false;
	}
}
