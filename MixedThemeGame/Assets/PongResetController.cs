﻿using UnityEngine;
using System.Collections;

public class PongResetController : MonoBehaviour {

	float timer;
	public GameObject succesfulGameOver;

	void OnEnable () 
	{
		timer = 2;
	}

	void Update () 
	{
		timer -= Time.deltaTime;

		if(timer <= 0)
		{
			gameObject.SetActive (false);
			succesfulGameOver.SetActive(true);
			succesfulGameOver.GetComponent<WhatsNextController>().ResetGame();
		}
	}
}
