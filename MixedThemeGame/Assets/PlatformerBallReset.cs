﻿using UnityEngine;
using System.Collections;

public class PlatformerBallReset : MonoBehaviour {

	public GameObject player;
	public GameObject levelStart;
	public PlayerInput playerInput;
	Vector3 startingBallPosition;

	void Awake ()
	{
		startingBallPosition = levelStart.transform.position;
	}

	void OnEnable()
	{
		playerInput.SetCurrentLevel(this.gameObject);
	}

	public void SetToStartingPosition()
	{
		player.transform.position = startingBallPosition;
	}
	

}
