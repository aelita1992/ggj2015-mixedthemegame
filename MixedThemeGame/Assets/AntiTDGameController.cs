﻿using UnityEngine;
using System.Collections;

public class AntiTDGameController : MonoBehaviour
{
	public GameObject crosshair;
	
	void OnEnable()
	{
		crosshair.SetActive(true);
	}

	void OnDisable()
	{
		crosshair.SetActive(false);
	}
}
