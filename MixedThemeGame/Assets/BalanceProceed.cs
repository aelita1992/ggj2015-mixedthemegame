﻿using UnityEngine;
using System.Collections;

public class BalanceProceed : MonoBehaviour {

	public GameObject fpsReference;
	public GameObject balanceReference;

	void OnEnable()
	{
		fpsReference.SetActive(true);
		balanceReference.SetActive(false);
	}
}
