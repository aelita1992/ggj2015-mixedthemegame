﻿using UnityEngine;
using System.Collections;

public class BalanceRestart : MonoBehaviour {

	public PlayerMovementBalance playerMovementBalance;

	void OnEnable()
	{
		playerMovementBalance.ResetBall ();
		gameObject.SetActive(false);
	}
}
