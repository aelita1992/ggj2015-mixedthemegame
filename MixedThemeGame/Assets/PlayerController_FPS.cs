﻿using UnityEngine;
using System.Collections;

public class PlayerController_FPS : MonoBehaviour
{
	public AK47Controller ak47Controller; 
	public GameObject fpsReference;
	public GameObject toBeContinuedReference;

	private static PlayerController_FPS pseudoSingleton;
	public static PlayerController_FPS Get()
	{
		return pseudoSingleton;
	}

	void Awake()
	{
		pseudoSingleton = this;
	}

	void FixedUpdate()
	{
		if (Input.GetMouseButton(0))
			ak47Controller.Fire();
	}

	void OnTriggerEnter(Collider other)
	{
		fpsReference.SetActive(false);
		toBeContinuedReference.SetActive (true);
	}
}
