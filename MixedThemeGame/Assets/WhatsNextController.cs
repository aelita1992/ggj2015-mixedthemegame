﻿using UnityEngine;
using System.Collections;

public class WhatsNextController : MonoBehaviour 
{
	enum ResetState {IDLE, SUCCESSFUL, LOST}

	public GameObject pongGame;
	public BallMovement ballMovement;

	public GameObject platformGameReference;
	public GameObject pongGameReference;

	ResetState state;
	float timer;

	void Awake()
	{
		state = ResetState.IDLE;
	}

	public void ResetGame()
	{
		timer = 2;
		state = ResetState.LOST;
	}

	public void ShowWinScreen()
	{
		timer = 2;
		state = ResetState.SUCCESSFUL;
	}

	void Update()
	{
		if(timer > 0)
			timer -= Time.deltaTime;

		if(timer <= 0 && state != ResetState.IDLE)
		{
			if(state == ResetState.LOST)
			{
				state = ResetState.IDLE;
				pongGame.SetActive(true);
				ballMovement.ResetBall();
				gameObject.SetActive(false);
			}

			if(state == ResetState.SUCCESSFUL)
			{
				platformGameReference.SetActive(true);
				pongGameReference.SetActive(false);
			}
		}
	}
}
