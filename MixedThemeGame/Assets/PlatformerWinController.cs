﻿using UnityEngine;
using System.Collections;

public class PlatformerWinController : MonoBehaviour {

	public GameObject platformerReference;
	public GameObject nextGameReference;
	
	void OnTriggerEnter2D(Collider2D other)
	{
		nextGameReference.SetActive (true);
		platformerReference.SetActive(false);
	}
}
