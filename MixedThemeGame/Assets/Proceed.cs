﻿using UnityEngine;
using System.Collections;

public class Proceed : MonoBehaviour {

	public GameObject fpsReference;
	public GameObject nextGameReference;

	void OnTriggerEnter(Collider other)
	{
		nextGameReference.SetActive (true);
		fpsReference.SetActive(false);
	}

}
